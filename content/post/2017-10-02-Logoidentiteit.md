---
title: Logo + identiteit
date: 2017-10-02
---

Na het bedenken van het spel zijn er al een aantal ideeën naar boven gekomen voor het ontwerpen van een huisstijl en het creeën van een logo. 
Er was al een onvderling bepaald dat we graag met veel kleur wilden werken. En dat nam ik mee met mijn ontwerpfase.
Tijdens de werk uren heb ik voornamelijk nagedacht en woorden zitten opschrijven die te maken hebben met het spel en de opleiding van de doelgroep.
Dit heb ik gedaan om een naam te kunnen bedenken voor het spel. Doormiddel van woorden op te schrijven en met de woorden te spelen en of te kunnen combineren. 
Ook zou er zo een slogan uit kunnen komen rollen die zich weer onder de titel van het spel kan bevinden. Ik heb hierna een selcectie gemaakt van woorden waar ik echt wat mee kon en heb zo 3 namen verzonnen waaronder:

- Contrivia
- Catrivia
- Collectit

Vervolgens ging ik met die woorden aan de slag en bedacht ik er een beeldmerk bij door in mijn dummy een aantal schetsen te maken die aansloten bij de opleiding en het spel.
Ik heb toen ik naar mijn idee voldoende schetsen had gemaakt de schetsen gedeeld met mijn team en besproken voor feedback. Ook deelde meteen alle anderen hun visie en of geschetse ideeën.

![alt tekst](../img/0210.jpeg)

