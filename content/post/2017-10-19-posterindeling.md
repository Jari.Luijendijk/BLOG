---
title: Poster indeling
date: 2017-10-19
---

Tijdens de taakverdeling heb ik er ook voor gezorgd dat ik de poster mag gaan maken, het is me gevraagd en ik ging er meteen mee akkoord.
Ik heb in mijn dummy allereerst wat schetsen gemaakt voor de indeling en ligging van de poster. Daarnaast heb ik bedacht wat voor elementen er allemaal in voor moeten komen, wat wil ik de klanten (gebruikers) nu echt laten zien.
Zo dacht ik aan vraagkaarten, bouw/bevestingskaarten, gebruik smartphone, enthousiasme gebruiker maar ook aan een passende slogan of quote.

![alt tekst](../img/posteraf.jpeg)

![alt tekst](../img/posterschets.jpeg)