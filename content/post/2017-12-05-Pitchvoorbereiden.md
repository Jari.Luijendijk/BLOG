---
title: Pitch voorbereiden
date: 2017-12-05
---

In de ochtend kregen we te horen dat we een paar uur later een pitch moesten houden om te oefenen. We hadden al op de planning gezien dat er in deze dagen een pitch gegeven zou moeten worden. 
Elliot wilde dat wel op zijn rekening nemen. Toen we te horen kregen dat we hem over een paar uur moesten oefenen, schrok ik een beetje. Ik wilde graag de pitch voorbereiden zodat al het benodige wordt verteld. 
Daarom pakte ik mijn schrift erbij en begon ik een hoofdlijn op te schrijven wat er allemaal in de pitch voor moet komen. Ook is de conceptomschrijving belangrijk binnen een pitch en wat het probleem is die wij willen oplossen.
Ook heb ik geleerd dat bij een pitch een openingszin de helft van de pitch kan bepalen. Je kan met een openingszin de luisteraar enthausiastmeren en zich iets laten afvragen.
Daarom heb ik ook een aantal openingszinnen verzonnen en opgeschreven in mijn dummy. De rest van het groepje had er geloof ik niet zo veel zin in en liet het maar over zich heen komen.