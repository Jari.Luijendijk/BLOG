---
title: Eigen input
date: 2017-10-10
---

Op deze dag waren al onze gedachtes weer vers en gingen we aan de slag met het bedenken van ieder onze eigen huisstijl. 
De bedoeling was om een styleguide te creeëren waarmee je kon aantonen wat jou ideeën zijn over de kleur, vorm, fonts etc.
Ook moest ieder zijn logo schetsen iets verder uitwerken om er een beter beeld bij te krijgen.
Hiervoor hebben we binnen het team een deadline vastgesteld zodat we de volgende keer elkaars werk kunnen waarnemen en feedback over kunnen vragen en geven.

![alt tekst](../img/styleguide.jpeg)