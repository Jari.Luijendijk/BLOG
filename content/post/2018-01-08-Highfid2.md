---
title: High-fid 2
date: 2018-01-08
---

Na het afronden van de pagina's vandaag heb ik besloten om het prototype clickable te maken. Ik heb hiervoor onderzoek gedaan hoe ik dit het best kon doen. 
Uit het onderzoek bleek dat Adobe Xd redelijk goed werkt om een clickable prototype mee te maken. Die heb ik geinstalleerd en daarin heb ik mijn bestanden toegevoegd.
Na het toevoegen van mijn pagina's van ons product ben ik pagina's en knoppen gaan linken met elkaar zodat ik door het product kan navigeren zoals ik zelf graag zou willen.
Door het telkens opnieuw te testen of alles goed navigeerd binnen het product kwam ik ook kleine foutjes tegen die ik zo kon herstellen door het op een andere manier te laten navigeren.

https://xd.adobe.com/view/48d58a31-ae1a-4cc3-8bfe-eaea05c3254f/

![alt tekst](../img/high2.jpeg)

![alt tekst](../img/high3.jpeg)