---
title: Verandering
date: 2018-05-15
---

Vorige week tijdens onze McDonalds bespreking hadden we voor mijn concept gekozen in samenwerking met het concept van Sem de Peinder. Nu deze ochtend Nikki langs kwam voor een kort gesprekje vroeg ze onder anderen naar onze vorderingen binnen het project.
We vertelden in welke fase we nu in het project zaten en droegen onze 2 in 1 concepten voor. Ze gaf zonder te vragen direct feedback op de concepten en vertelde dat mijn concept te weinig beweging zou bevatten.

Omdat mijn concept door Nikki is afgewezen zijn we met zijn allen compleet aan het focussen gegaan op het concept van Sem de Peinder. Namelijk het afvalverwerkings concept doormiddel van doeltjes op straat.
Ik moest even omschakelen van gedachtengang over het concept en kwam al direct met een aantal aanvullingen op het huidige concept van Sem.

Daarnaast heb ik de studio ochtend benut om deskresearch te doen naar afval + verwerking in Rotterdam -> Afrikaanderwijk.
Hebben we het concept besproken en wat we er allemaal voor moeten maken. We hebben de taak gekregen doormiddel van lootjes te trekken met daarop de opdracht die jij toegedient krijgt.

Ik kreeg de opdracht:

- info tegel
- logo
- low-fid prototype