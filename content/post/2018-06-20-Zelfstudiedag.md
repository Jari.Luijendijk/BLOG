---
title: Zelfstudiedag
date: 2018-06-20
---

Op deze woensdag stond er een zelfstudiedag ingeroosterd. Deze dag heb ik ingericht om onderzoek te doen voor mijn concept.
Ik heb hiervoor in de regio van mijn vastgestelde locatie (regio Voorne Putten) onderzoek gedaan naar verschillende locaties, de doelgroep en horeca rondom.

Door achter bepaalde vragen te komen heb ik de volgende onderzoeksmethoden gebruikt:
- Interviewen (horeca)
- Shadowing (bezoekers/toeristen)
- Literatuuronderzoek (deskresearch/web/magazine)
- Mindmapping (juiste locaties voor terminals)
- Foto studies (om locaties vast te leggen en om een beeld te krijgen van de doelgroep)

Daarnaast ben ik begonnen met het schetsen van mijn onderzoeksposter en heb ik deze later op de avond uitgewerkt.