---
title: Conceptfase II
date: 2017-09-27
---

Op deze dag draaide alles om het bedenken van functies van het spel. Met hoe iets daadwerkelijk goed zou kunnen werken en op wat voor manier.
De hoofdonderdelen waren al zo goed als bedacht maar niet hoe het in zijn werk zou kunnen gaan. Er waren veel discussies onderling met veel verschillende meningen.
Sommige waren hevig in een strijd terwijl ze eigenlijk allebei hetzelfde bedoelden. In die tussentijd was ik bezig met het bedenken over hoe wij verschillende categorieën konden toevoegen aan het spel.
Voornamelijk hoe je bij zo'n categorie terecht komt. Ik dacht dat een kleurenrad hierbij wel van toepassing zou kunnen zijn en heb ik in mijn dummy een aantal kleine eenvoudige schetsen gemaakt.
Daarna heb ik deze schetsen voorgedragen aan mijn team toen het weer even wat rustiger was. Ze vonden het aan de ene kant een goed idee maar er waren ook andere ideeën, bijvoorbeeld om een categorie te koppelen aan een locatie.
Daarover hebben we de rest van de middag gediscuseerd, en ook nog over andere onderdelen van het spel zoals de bouw/bevestigingskaarten.