---
title: Ontwerpcriteria
date: 2017-11-28
---

Op deze dinsdag hebben we het wekelijkse gesprek ingeroosterd en vertellen we onze ups and downs aan onze teamleden. Over het algemeen ging het nu wel goed en liggen we aardig op schema. 
Ook heeft iedereen voor vandaag concepten bedacht en daarvan een low-fid prototype gemaakt. Die presenteerde we aan elkaar en bespraken we achteraf met de positieve en negatieve punten van het concept.
Nadat iedereen zijn concept had gepresenteerd probeerden we van alle concepten, 1 goed concept te maken wat naar ons idee goed zou kunnen werken.
Later op de dag ben ik bezig geweest met mijn persoonlijke opdracht, namelijk de ontwerpcriteria. Terwijl ik er mee bezig was kregen we plotseling een minimalistische uitleg over de ontwerpcriteria met wat er in terug te vinden moet zijn.

![alt tekst](../img/ontwerpcriteria.jpeg)