---
title: Iteratie III nu toch echt van start!
date: 2017-10-13
---

De grote dag van de uitwerkfase is begonnen! Vandaag zal de taakverdeling worden gemaakt, de huisstijl worden uitgekozen en de ideeën van de logo's worden vastgesteld.
Ik bracht mijn styleguide meteen op tafel en er waren goede reacties over. We hebben ze met elkaar vergeleken en we zijn met die van mij verder in uitwerking gegaan. 
De naam en de logo schets die ik had bedacht waren uiteindelijk niet gekozen maar er stonden mooie van mijn teamgenoten tussen. Met het logo gecombineerd van twee andere teamleden en de styleguide van mij zijn we begonnen aan het vormgeven van het spel.

Bij de taakverdeling heb ik gekozen om de bouw/bevestiginskaart uit te werken binnen de gekozen stijl. Vooraf wat schetsen en daarna uitwerken in Illustrator. Ik heb achteraf de teamgenoten die de styleguide niet zo goed begrepen verteld
wat een styleguide is, hoe je er een moet maken en hoe je hem moet gebruiken om er producten mee te maken. Het belangrijkste wat ik vertelde was dat iedereen zich moet houden aan de kleurcode's, fonts, vormen en grafische stijl.