---
title: Validatie High-fid Prototype
date: 2018-01-16
---

Vandaag moesten we ons High-fid Prototype laten valideren. Hiervoor hoefden we ons niet in te schrijven en konden we deelnemen vanaf 13:00 uur.
Toen het 13:00 uur was moesten we ons alsnog inschrijven op een formulier dat werd opgehangen en waren we om 14:15 aan de beurt. 
Voordat Elliott en ik gingen valideren waren we nog hard bezig met het aanpassen van onze prototypes. We hebben meer klik-functies gemaakt zodat het prototype vollediger is.
Toen we uiteindelijk aan de beurt waren verliep onze validatie als gepland.
We hadden in 1 keer een goedkeuring, maar wel wat kleine aandachtspunten.

Prototypes:

https://xd.adobe.com/view/48d58a31-ae1a-4cc3-8bfe-eaea05c3254f/

https://xd.adobe.com/view/65831d8c-453e-4f84-94bf-83b99a3a88b4/screen/8d3bd594-a201-4763-ba5e-e7ef9d11e0f8/Web-1920-24

