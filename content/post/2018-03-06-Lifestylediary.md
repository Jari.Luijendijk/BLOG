---
title: Lifestyle diary
date: 2018-03-06
---

In de vakantie hebben we ieder voor zich gewerkt aan onze eigen 48uurs lifestyle diary. 
Ieder moest bijhouden wat hij eet, doet aan beweging en wat voor prikkels hij/zij krijgt van verschillende mediabronnen.

Na dit te hebben bijgehouden moesten we dit visualiseren met bijvoorbeeld knip en plak werk. Maar ook een duidelijke route door de 48uur en daarbij de acties aangeven en de daarbij toebehorende emoties.
