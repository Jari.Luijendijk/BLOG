---
title: Testplan
date: 2018-01-09
---

Vandaag ging ik het testplan maken voor het testen van ons high-fid prototype. Ik heb hiervoor het eerder opgestelde testplan gebruikt die gemaakt was voor het testen van het Low-fid prototype.
Ik heb hierin vermeld hoe we het gaan testen, met wie of wat en met wat voor materiaal.

![alt tekst](../img/testp1.jpeg)

![alt tekst](../img/testp2.jpeg)