---
title: Design challenge II
date: 2017-11-24
---

Vandaag hebben we onze groepjes gepresenteerd en voorgesteld aan de klas. Onze motivatie voorgedragen en laten zien waar wij als groepje voor staan. Later zijn we gezamenlijk aan de slag gegaan met het maken van de user-journey. Voorbereid met allemaal post-its en van begin tot eind uitgeschreven wat iemand meemaakt met het uitgaan. Van zowel voordat de avond begint, tijdens en na afloop.