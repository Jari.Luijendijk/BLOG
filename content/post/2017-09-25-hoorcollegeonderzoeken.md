---
title: Hoorcollege onderzoeken
date: 2017-09-25
---

Hierbij een aantal punten die deel uitmaken van een goede ontwerper met een goed ontwerpproces en een belangrijke houding waarbij interpreteren een belangrijke rol speelt.

HOUDING

- Nieuwsgierig
- Pro-actief
- Open
- Kritisch
- Gericht op inzicht
- Bereid om oordeel uit te stellen
- Routines kritisch bevragen
- Kennis waarderen
- Inzicht willen delen
