---
title: Studiodag
date: 2018-06-21
---

Deze dag was voor mij een hele productieve dag.

In de ochtend heb ik mijn onderzoeksposter gepresenteerd aan twee medestudenten. Volgens de roddel techniek heb ik feedback mogen opnemen over mijn presentatie en poster.
Na het mogen noteren van mijn gekregen feedback heb ik een transfer geschreven, met daarin leerpunten en middelen die ik mee zou kunnen nemen gedurende mijn concept project.

Hierna heb ik een aantal verschillende docenten gevraagd om feedback te geven op een aantal onderzoeksdeliverables die ik eerder deze week had gemaakt.
Zoals een concurrentie analyse, ontwerpdoel en een onderzoeksplan.

Na de tweede presentatie van deze dag moesten we ons voorbereiden op een creatieve sessie die wijzelf moesten organiseren. Zo moesten wij 2 divergerende technieken voorbereiden en 1 convergerende. 
Nadat ik deze technieken had voorbereid heb ik 2 medestudenten benaderd om samen met hun de creatieve technieken uit te voeren. De medestuden waren iets wat onzeker over het organiseren van zo'n creatieve sessie.
Daarom nam ik het voortouw en begon ik met het inrichten van een creatieve ruimte en sloot ik aan met een divergerende sessie.

Nadat we met zijn 3en in totaal 9 creatieve sessies hebben uitgevoerd, ben ik er wel achter gekomen wat voor mijzelf een handige techniek is om tot een selectieve beslissing te komen voor mijn concept.