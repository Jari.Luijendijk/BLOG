---
title: Summerschool week II
date: 2018-06-25
---

Vandaag heb ik een ontwerpcriteria gemaakt. Dit heb ik gemaakt om tijdens het maken van schetsen en prototypen er bij te houden en mijzelf te controleren of ik mij wel aan de opgestelde criteria houdt.
Om te bepalen waarom ik bepaalde keuzes binnen mijn ontwerp maak heb ik een Design Rationale gemaakt. Hierin laat ik zien wat de keuzes zijn en waarom.
Om 09:45 heb ik al feedback gevraagd aan bob, tussentijds ben ik nog een keer langs geweest maar na 2 uur door te hebben gewerkt (te zitten wachten) ben ik naar Nikki gegaan om feedback te krijgen.

Nikki gaf mij goede feedback op mijn gemaakte User Journey en kreeg ik zelf een inzicht hoe ik het zou moeten aanpassen. Daarom ben ik aan de slag gegaan met een User journey 2.0.