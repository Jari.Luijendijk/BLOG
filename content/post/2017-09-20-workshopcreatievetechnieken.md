---
title: Workshop creatieve technieken
date: 2017-09-20
---

Vandaag had ik mijn eerste workshop, namelijk Creatieve technieken: Brainstormen. Hierin werd uitgelegd wat brainstormen nou daadwerkelijk inhoud en wanneer je brainstormen gebruikt. 

De Brainstormtechniek:

- Vereist een goede voorbereide procedure.
- Vindt niet plaats zomaar tijdens de koffie.
- Is een techniek die werkt in groepen.
- Laat alles toe in een brainstormsessie en gooi niks weg.
- Het gaat om het verzinnen van ideeën en daarna het oordelen over de ideeën. (uitstel van oordeel)

Een brainstorm sessie wordt gehouden in 2 fases. Fase 1 (de groenlicht fase) waar alle ideeën gegenereerdd en geleverd worden. In deze fase mag er absoluut geen commentaar worden overgebracht op de geleverde ideeën.

Fase 2 (de roodlicht fase) in deze fase kunnen de ideeën kort worden toegelicht door de inbrenger. Ook is deze fase bedoelt voor groepsleden die elementen aan hun voorkeur willen toevoegen. De goede ideeën krijgen dan ook de meeste toevoegingen
Het is handig om vooraf een voorzitter, notulist, tijdsduur en een probleem formulering vast te leggen.