---
title: Design Challenge 1st ideas
date: 2017-09-05
---

Vandaag de eerste ideën om gegooid en begonnen met het bedenken van een nieuw spel, mijn nieuwe idee is ontstaan in de terugweg naar huis in het OV. De gedachte was om back to basic te gaan voor onze gekozen doelgroep, namelijk de bouwkunde studenten. 

IDEAS: 

- Offline gedeelte

- De bedoeling is doormiddel van het uitvoeren van eenvoudige opdrachten de basis te leren kennen over materiaal en bepaalde constructies te voltooien.

- Probeer de opdrachten zo goed, stevig, snel en mooi mogelijk uit te voeren en behaal de hoogste scores. Door een opdracht goed uit te voeren wordt je beloond met middelen die je nodig zult hebben in het volgende spel.

Mijn idee heb ik meteen de volgende dag voorgedragen aan mijn werk team.
