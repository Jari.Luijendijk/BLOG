---
title: Studiodag + Inspiratielezing
date: 2018-04-19
---

Nu ons nieuwe kwartaal van start is gegaan, maakte ik een nieuwe map aan voor in onze teamdrive. Het was een redelijk rommeltje en moest nodig opgeruimt worden.
Zo heb ik voor OP3 en OP4 een aparte map aangemaakt en heb ik daarin verschillende mapjes aangemaakt met deelonderwerpen en verschillende fases van het proces.

Na een korte ochtend in de studio zijn we als incompleet team, de straat op gegaan om fieldresearch te doen door onze wijk. We hebben ieder een aantal foto's gemaakt om later te gebruiken voor reflectie en moodboard.

Na de fieldresearch te hebben voltooid ging ik naar een inspiratielezing over gedragsverandering. Dit werd gehouden in de collegezaal van de WDKA door een Portugese leraar.
De inspiratielezing was erg indrukwekkend en hij presenteerde op een leuke interactieve manier. 
Hij presenteerde zijn verhaal op een manier die goed aansloot bij onze studie, zodat wij bij het verhaal betrokken bleven.

