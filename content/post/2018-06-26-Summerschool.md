---
title: Summerschool - Making
date: 2018-06-26
---

Ik begon de ochtend met feedback te vragen aan Jan10 over mijn Design Rationale. Na dit te hebben genoteerd heb ik snel daarna de feedback verwerkt en een versie 2.0 gemaakt.
Ook bleek mijn ontwerpcriteria niet voldoende te zijn en heb ik netzoals bij de Design Rationale de feedback verwerkt in mijn ontwerpcriteria 2.0
Aansluitend kon ik mij bezig gaan houden met het uitwerken van mijn concept. Ik starte met het maken van een soort sitemap voor mijn dockingstation die ik had gekozen om uit te gaan werken.
Daarna heb ik een aantal wireframes gemaakt die een indeling moesten verbeelden voor mijn dockingstation interface. Omdat ik niet wist of ik hiermee goed op weg was heb ik ook dit laten voorzien van feedback.
Tijdens de feedbacksessie bleek het inderdaad niet de goede manier te zijn, hoe ik het had bedoeld. Nadat ik inzichten heb gekregen in hoe ik het wel moet aanpakken ben ik eerst van start gegaan met het schetsen van verschillende schermen in mijn schetsschrift.
Daarna heb ik uitgetekend in een correcte sitemap hoe ik wil dat mijn pagina's in elkaar overlopen. Toen ik 's avonds thuis kwam heb ik verschillende concrete wireframes getekend die direct mijn Low-fid prototype vormde. 
Ik heb direct mijn Low-fid getest bij een aantal verschillende mensen en hier een testrapportage over geschreven.