---
title: The Brainstorm
date: 2018-03-23
---

Vandaag dan eindelijk de geplande brainstromsessie gehouden. We maakte onze eigen brainstormruimte, met goed zittende stoelen, schrijftafel, whitebord en voldoende pen en papier.
Om de sessie van start te laten gaan had Sem de Peinder bedacht om eerst wakker te worden met een spelvorm. We zaten met zijn vieren in een kring en deden eerst een woorden slang. 
Zo moesten we om de beurt een woord zeggen en soms sloot dat op elkaar aan maar soms ook niet.
Er was nog ruimte voor een 2e spelvorm en bedacht ik een soortgelijk spel in de trend.
Namelijk het dieren spel, een spel waarbij je een dier moet noemen en met welke letter het woord eindigt moet het volgende woord beginnen. Ook dit maakte onze hersenen goed wakker om aan de officiele brainstormsessie te beginnen.

Sem de Peinder organiseerde een A3 methode,

Daarnaast werden de ideen van elkaar besproken en werden de besten genoteerd op het whiteboard.
Al snel verzonnen we er nog een aantal punten bij en werden de concepten uit verschillende perspectieven benaderd.

Als afsluiting kozen we de 4 beste concepten uit en noteerde wij die op een A3 vel om ze later nog een naast elkaar te houden.