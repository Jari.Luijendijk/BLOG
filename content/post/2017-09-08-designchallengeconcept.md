---
title: Design challenge concept
date: 2017-09-08
---

Vandaag gingen we de planning maken en hebben we de taken verdeeld en heeft ieder voor zich zijn favoriete bezigheden uitgekozen of hetgene waar die gene goed in is. Eerder deze week heeft ieder voor zich een collage en een moodboard gemaakt, we hebben ze naaste elkaar gehouden en zo de beste punten van ieder opgenoemd en genoteerd. Ook hebben we gelijk elkaar feedback gegeven op onze individuele werken.

Mijn taak van de taakverdeling is om het offline gedeelte van het spel idee uit te schrijven, het bedenken van de offline spelopdrachten, nadenken over eventueel materiaal dat nodig is. Ook het schetsen voor het online gedeelte heb ik op mij genomen samen met een teamlid. Eerst ieder voor zich schetsen en uiteindelijk naast elkaar houden en overwegen.

![alt tekst](../img/collage.jpeg)

![alt tekst](../img/moodboard.jpeg)