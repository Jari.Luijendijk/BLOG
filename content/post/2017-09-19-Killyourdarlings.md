---
title: Kill your darlings
date: 2017-09-19
---

Vandaag kregen we te horen dat we ons onderzoek en onze concepten volledig over boord moesten gooien, en opnieuw een spel moesten gaan bedenken, en er nog dieper op in te gaan. Hierbij komt ook meer onderzoek kijken en we moeten als groep ons meer verdiepen in het thema en doelgroep. Ook moeten we gaan bedenken hoe we de doelgroep het spel laten spelen en dat ze het spel blijven spelen met een bepaalde drang. Deze dag kregen we dus te horen dat iteratie II is begonnen. De nieuwe opdrachten werden ons ook bekend gemaakt voor deze iteratie en is al gelijk een stuk duidelijker dan bij iteratie I.

![alt tekst](../img/killyourd.jpeg)