---
title: Summerschool
date: 2018-06-09
---

Helaas is het mij niet gelukt om met het einde van OP4 al mijn 8 competenties te halen.
Hiervoor volg ik nu de herkansing module van OP4 genaamd "Summerschool". 

Vandaag is de kickoff gepresenteerd en hebben we een briefing gekregen met de opdracht voor komende twee weken.
Aansluitend kregen we direct een opdracht in de klas op een longlist te maken voor onze: Doelgroep, locatie en een bedrijf waarmee wij het project willen gaan aanpakken.

Als huiswerk hadden we meegekregen om een shortlist te maken van de longlist en om een definitieve keuze te maken. Zodat wij dit de volgende dag konden voordragen aan de docenten.