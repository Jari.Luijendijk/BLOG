---
title: Richting concept
date: 2018-06-22
---

Vandaag prestenteerde ik met alweer de roddeltechniek mijn conceptposter. Ik vroeg allereerste 2 medestudenten voor feedback op de poster, maar omdat ik ook feedback wilde van
mensen die langer in het vak zitten vroeg ik ook mijn vakdocenten Nikki en Fedde om feedback. Zowel over de inhoud als de vorm kreeg ik positieve en opbouwende feedback waar ik wat aan heb.

Ook kreeg ik wat feedforward van Nikki, waar ik in de komende tijd op moet letten binnen het ontwerp en ontwikkelen van mijn concept.

Later ben ik bezig geweest met het opstellen van mijn testplan in samenwerking met mijn conceptposter. 