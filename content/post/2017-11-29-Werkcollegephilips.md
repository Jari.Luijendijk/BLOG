---
title: Werkcollege - Philips
date: 2017-11-29
---

We bekeken vandaag tijdens werkcollege een trailer van de Philips wake-up light. Na het bekijken van de video moesten we er een poster voor maken en alle systemen en theorieën benoemen die tot uiting komen bij de speciale wekker lamp.
Deze systemen hebben we maandag tijdens het hoorcollege te horen gekregen en moesten we vandaag tijden het werkcollege toepassen.