---
title: Richting het einde van Summerschool
date: 2018-06-27
---

In de ochtend zocht ik direct weer een docent op die mijn testrapportage van feedback kon voorzien. Dit maal kon Bob feedback geven over mijn product. Met de benodigde informatie
kon ik verder met het aanpassen van mijn testrapportage.
Daarna heb ik alle 4 mijn nog niet behaalde competenties opgeschreven met daarbij een checklist met deliverables die ik al wel/niet heb afgerond. Hiermee liep ik even naar Nikki of ik met deze deliverables goed kan aantonen of ik competent genoeg ben.
Na het gesprek bleek, dat wanneer ik de deliverables zou afronden ik een grote kans had om competent te zijn. Nu alleen nog de bewijzen toevoegen en er correcte starrts over kunnen schrijven.

Om mijn Verbeeld en uitwerking kwaliteiten voor te bereiden voor het maken van mijn High-fid prototype, heb ik vooraf een styleanalyse van mijn gekozen bedrijf "Rabobank" gemaakt.
Aansluitend heb ik hieruit een styleguide gemaakt met juiste kleuren, fonts, vormen en andere elementen die ik kon gebruiken in de schermen van mijn High-fid prototype.
Na het maken van de styleguide ben ik aansluitend verder gegaan met het maken van de eerste schermen gebaseerd op de wireframes van het low-fid prototype.