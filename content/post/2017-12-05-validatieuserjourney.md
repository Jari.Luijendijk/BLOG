---
title: Validatie User-Journey
date: 2017-12-05
---

Nadat Elliott de User-journey heeft uitgewerkt die de rest van het team heeft uitgestippeld. Was het de taak aan mij en Lotte om deze te laten valideren.
Toen wij er eens voor gingen zitten om de User-journey te begrijpen, snapte we eigenlijk niet direct waar het over ging en hoe die precies werkt.
Om dat te laten valideren was dan ook wat lastiger dan normaal.
Uiteindelijk hebben we er feedback over gekregen en werd ons een tip gegeven over hoe we de User-journey overzichtelijker konden maken.