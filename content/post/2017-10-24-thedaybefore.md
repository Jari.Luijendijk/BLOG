---
title: The day before..
date: 2017-10-24
---

Deze dinsdag dachten we even de laatste puntjes op de i te zetten en daarna snel af te kunnen sluiten.
Dit bleek voor ons niet het geval te zijn. Er moest nog veel gebeuren waaronder een proceskaart, procesboek met deliverbles, bouw/bevestingskaarten bijwerken en ieder zo zijn eigen kleine aanpassingen waar hij/zij de taak voor had.
Ik heb uiteindelijk de taak op mij genomen om het procesboek te vullen met deliverbles en daarbij bijbehorende tekst. Daarbij kwam ook kijken dat ik het redelijk netjes en overzichtelijk wilde opmaken waar aardig wat tijd in gaat zitten.
Het procesboek heb ik in de avonduren buiten school voltooid omdat dit de volgende dag 09:00 ingeleverd moest worden. Overdag hebben we in groepsverband besproken wat er de volgende dag tijdens de expo allemaal moet worden meegenomen en hoe we het gaan presenteren.
Ik moest dingen meeenemen die terugkoppelde aan het spel, zoals een schaar (bevestigingsmateriaal) en een boek (bouwmateriaal). Ook heb ik de bevestiging/bouwkaarten aangepast naar het juiste formaat die overeenkomt met de door ander gemaakte vraagkaarten.
Dit moest vlot gebeuren omdat ze diezelfde dag nog geprint moesten worden.

![alt tekst](../img/pro1.jpeg)

![alt tekst](../img/pro2.jpeg)

![alt tekst](../img/pro3.jpeg)