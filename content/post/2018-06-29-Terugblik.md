---
title: Terugblik Summerschool
date: 2018-06-29
---

Nikki heeft mij mondeling in de loop van de weken 2x benaderd over mijn ervaring van Summerschool. Deze ervaring is een klein beetje veranderd op een positieve manier.
Doordat ik aan de herkansing heb deelgenomen heb ik een beter inzicht gekregen van wat ik allemaal aan het doen ben op de opleiding CMD. 
Zo heeft Summerschool mij laten zien wat er allemaal wel niet nodig is om tot een goed aansluitend eindproject te komen. 

Ook is het helder dat feedback vragen zeer van belang is, TUURLIJK VAL IK OP MIJN BEK, maarja dat is nu eenmaal de bedoeling als ontwerper.
Ik heb geleerd dat niet alleen mijn inzicht teld, want vaak zie ik mijn eigen valkuilen over het hoofd, die anderen weer wel opvallen.
In dit kwartaal ben ik er ook veel voor anderen geweest, ik heb andere veel geholpen en een aantal keer heb ik ook als voorbeeld gediend. Ik vond het deze keer ook prettig
om met anderen te overleggen. Misschien komt dit wel doordat ik voornamelijk veel zelf aan de slag was, ik weet het niet, het voelde in iedergeval goed.

Ik denk dat ik mijzelf op verschillende gebieden in deze korte periode op grote wijze heb weten te ontwikkelen. Ik ben nog actiever geweest, ik heb bijna iedere keer om feedback gevraagd en verwerkt
en ik heb voor mijn gevoel ook een heel proces doorlopen, wat minder tot uiting kwam wanneer ik in een teamverband aan het werk was. 
Ik heb de gehele Summerschool gevolgd en gedaan wat mijn werd opgedragen, zo heb ik zelfs elementen gedaan die ik al had behaald. Die heb ik bewust gedaan om uiteindelijk tot een goed concept te komen en soms hielp ik er ook nog eens medestudenten mee.

Al met al heb ik een positieve terugblik op mijn herkansing tijdens Summerschool.