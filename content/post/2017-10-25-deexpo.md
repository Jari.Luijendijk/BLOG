---
title: De EXPO
date: 2017-10-25
---

Aan al het mooie komt een einde en zo ook de eerste Design Challenge. 

Vandaag de dag worden door alle 1e jaars CMD-studenten hun spel concepten/uitwerkingen gepresenteerd doormiddel van een expositie.
Mijn rol tijdens de expositie was voornamelijk het vormgeven van de presentatietafel en het aantrekkelijk maken voor het oog. Anne en Katja waren de twee pitchers voor vandaag.
Zij vertelden aan ieder geïnteresseerde student/docent het doel van het spel doormiddel van een pitch. De rest van ons team ging rond bij de andere teams om hun expositie te bewonderen.
Ook konden wij meteen andere studenten naar onze stand doorverwijzen. 
Het was een leuke en leerzame ochtend, omdat je andere vaardigheden ziet en andere denkwijze.
Je ziet andere mensen pitchen en je ziet duidelijk verschil in kwaliteit van concept en uitwerking tussen de teams.

![alt tekst](../img/overzicht.jpeg)

![alt tekst](../img/layout.jpeg)

![alt tekst](../img/cards.jpg)