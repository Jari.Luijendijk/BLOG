---
title: Peerfeedback moment
date: 2018-05-29
---

Op deze studiodag hebben we besloten om een peerfeedback moment te houden en de bijlages over elk teamlid in te vullen. 
Daarnaast ben ik naar Nikki geweest om de validatie van ons PVA in OP3 aan te vragen.

Toen ik zag dat Elliott druk bezig was met zijn app prototype begeleidde ik hem hierin, hij zat namelijk vast met een onderdeel en doordat ik hem bijsprong kon hij daarna gemakkelijk verder werken.
Ook gaf ik hem nog wat opbouwende kritiek op het gebied van vormgeven waar hij wat aan had.

Hierna ben ik samen met Thomas, ons kartonnen prototype gaan testen.