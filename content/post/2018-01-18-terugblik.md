---
title: Terugblik OP2
date: 2018-01-18
---

Als ik terug kijk naar afgelopen periode, kijk ik terug op een moeizaam maar geslaagde periode. Het overleg binnen het team was minimaal met zijn allen.
Onderling werd er wel genoeg overlegd, maar iedereen werd niet van alle omstandigheden van te voren op de hoogte gesteld. 
Als ik terug kijk naar het eerste kwartaal en het vergelijk met afgelopen kwartaal, verliep de samenwerking in het eerste kwartaal naar mijn mening beter.

De tweede periode was een periode met veel lesuitval, onduidelijkheden, pittige theorie en ongeorganiseerde validatie momenten.
Ook was het een periode waarbij we al veel meer zelf initiatief moesten tonen om overal achter te komen, zowel lesstof als binnen de Design Challenge.

Al met al is het ons met het team gelukt om een geslaagd product (prototype) te op te leveren en hopelijk later te mogen presenteren in Paard.

Want dat, dat was ons grootste doel!