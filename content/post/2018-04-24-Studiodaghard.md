---
title: Studiodag + hard optreden
date: 2018-04-24
---

Aan het begin van deze studio ochtend zijn we met het team gaan bespreken wat er vandaag op de planning staat. Zo koos ik de taak om wat ontwerpvragen op te stellen, daarnaast zijn we met het team aan het dicussieren gegaan over de onderzoeksmethode.
Ook deelde ik en Sem de Peinder mee hoe de fieldresearch was verlopen.

Ik had ook even een momentje met Mieke, waarin ze vroeg hoe het met mij gaat en hoe het loopt met school. Veder was er niet veel belangrijks te bespreken.

Rond de middag kreeg ik een mailtje van de workshopgever over dat de workshop die ik vanmiddag zou gaan volgen niet door zou gaan in verband met te weinig animo.
Ik heb direct een mailtje terug gestuurd en laten weten dat ik het hier niet mee eens ben. Omdat dit mij al een aantal keer meer is gebeurt. Uiteindelijk leverde het mailtje goed resultaat op en ging de workshop waarvoor ik mij had ingeschreven gewoon door.
