---
title: Workshop testplan
date: 2018-01-09
---

Vandaag volgde ik de workshop "testplan". Het hoofddoel van deze workshop was voornamelijk, het uitvoeren van je test. Hoe doe je dat en met wat voor middelen.
Je moet zorgen dat je geen sturende vragen steld tijdens het testen.
Je moet tijdens een test goed observeren en de gebruiker niet te veel sturen.

Ook is het belangrijk wat de gebruiker "doet" omdat dat niet altijd hetzelfde is als wat de gebruiker "zegt".

Na de instructie en informatie die we tijdens de workshop kregen. Was het daarna de tijd om het zelf uit te voeren. Ik stelde mij als enige beschikbaar en kon ik zo oefenen met het testen van mijn prototype.
Tijdens het testen observeerde ik de gebruiker goed en wist ik precies waar nog onduidelijkheden waren.
Nadat ik het getest had met de gebruiker samen, kreeg ik feedback van de docent en kon ik goed gebruiken tijdens de test zelf.
