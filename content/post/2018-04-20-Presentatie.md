---
title: Presentatie
date: 2018-04-20
---

Vandaag werd ons Plan van Aanpak gepresenteerd door Elliott. Er was van te voren een schema gemaakt met tijden waarop de teams aanwezig moesten zijn om te kunnen presenteren. 
Wij waren ruim op tijd en waren klaar om te presenteren. Helaas moesten wij een uur wachten voordat we eindelijk aan de beurt waren. Er waren op onze tijd twee andere teams gegaan (voorgepiept) terwijl die helemaal niet aan de beurt waren.
Ik had er al weer helemaal de pik in omdat wij de dupe daarvan zijn. 
Toen wij eindelijk mochten presenteren hield ik mijn notitieschrift en pen bij de hand om de feedback en andere criteria te noteren.
Die over het algemeen erg posiftief was.

Nadat wij gepresenteerd hadden, moest een ander team presenteren. Over die presentatie moest ons team feedback geven. Door deze methode te gebruiken om over en weer feedback te geven, leer je ook om anders te kijken/luisteren naar een presentatie of uitleg.
