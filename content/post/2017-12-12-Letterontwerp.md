---
title: Workshop Letterontwerp
date: 2017-12-12
---

Vandaag volgde ik de workshop "letterontwerp", met deze workshop kregen we les over het maken van lettertypes en fonts.
Bij een lettertype moet je rekening houden met de afstanden tussen de letters maar ook tussen de stokken en lussen van letters.
Het is redelijk lastig om een eigen lettertype te ontwikkelen dat precies bij elkaar past en waar de spatiering goed is.

Eerst moesten we een aantal letters en woorden oefenen in hokjespapier. Om verschillende manieren van lettervormen te creëren.
Daarnaast oefende we met dikke zwarte stiften verschillende kalligrafie letters, wat ook weer een hele kunt opzich is.