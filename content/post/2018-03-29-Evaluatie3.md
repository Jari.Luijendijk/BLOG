---
title: Evaluatie OP3
date: 2018-03-29
---

Als terugblik op OP3 kijk ik terug op een moeizame periode. Dit komt omdat maar een enkele keer het team compleet was. 
Dit leverde veel miscommunicatie op, ookal werd iedereen redelijk op de hoogte gehouden via onze teamapp.

Omdat er niet veel deliverables verplicht waren om in te leveren, moesten we er aantal zelf verzinnen. Dit was niet voor iedereen even geschikt.
Ik heb veel taken op mij genomen waar veel tijd in ging zitten. De taken hadden beter verdeeld moeten worden zodat we als team afhankelijk van elkaar zijn en niet van 1 a 2 teamleden.

Ik heb in OP3 veel nieuwe dingen geleerd op het gebied van prototyping en onderzoeksmethodes.