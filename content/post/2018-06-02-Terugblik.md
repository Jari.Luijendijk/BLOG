---
title: Terugblik kwartaal IV
date: 2018-06-02
---

Als ik terugblik naar het afgelopen kwartaal dan blik ik terug op een ingewikkeld kwartaal.

Dit kwam omdat de samenwerking binnen het team niet altijd even soepel verliep. Dit kwam voornamelijk door veel afwezigheid van teamleden. Zo konden we niet overleggen wanneer we dit wilden en 
werden er vaak afspraken van de planning niet nagekomen. Nu het kwartaal tegen zijn einde liep, merkte ik dat er stiekem toch steeds meer werd gedaan door het team.
Iedereen kwam steeds meer opdagen en er werden producten geleverd, die we nodig hadden binnen het proces. 

Doordat iedereen op het laatst toch zijn goederen had geleverd, kon de eindexpo gelukkig toch nog voor ons plaats vinden. Ik had er namelijk allereerst geen vertrouwen in, dit kwam omdat ik mijn handen van de uitwerkingen had afgehouden.
Ik wilde namelijk ook niet voor paal staan tijdens mijn pitch gedurende de expo.

Gelukkig heb ik alles goed bij elkaar weten te rapen en heb ik doormiddel van goede aankleding van de stand en een heldere pitch te geven een goed resultaat weten neer te zetten.
Zo heb niet alleen ik maar ook het gehele team zich weten te plaatsen voor de beste acht kandidaten.