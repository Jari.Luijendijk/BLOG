---
title: Schets -> Ontwerp
date: 2018-01-03
---

Vandaag ben ik begonnen met het overnemen van de schetsen voor het high-fid prototype. Het is mijn taak om het high-fid onderdeel te maken voor op de locatie zelf.
De schetsen ben ik gaan uitwerken in Illustrator als high-fid prototype met vormgeven binnen de gekozen stijl die ik heb gemaakt. Ook heb ik zitten nadenken of er nog andere mogelijke schermen van toepassing zijn voor bij het interactieve middel.

![alt tekst](../img/low1.jpeg)

![alt tekst](../img/low2.jpeg)