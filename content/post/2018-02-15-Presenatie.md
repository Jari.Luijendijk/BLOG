---
title: Presentatie
date: 2018-02-15
---

Vandaag presenteerde we onze plan van aanpak, doormiddel van een diapresentatie. Er waren vandaag maar weer 3/5 teamleden aanwezig en was het moeilijk om de verdeelde taken voor ieder te bespreken.
Ieder had wel thuis aan zijn onderdeel gewerkt om het in de presentatie te zetten maar toen het feedback moment kwam, kon de feedback niet direct gegeven worden aan de personen die bepaalde dia's hadden voorzien van informatie.

Verder hebben wij met die aanwezig waren alle feedback verzameld en opgenomen en hebben we het met de teamleden besproken. We wisten van te voren dat we nog het een en ander misten wat concreet was. Daar moesten we aan gaan werken zoals een concretere planning + taakverdeling.
Na het besproken te hebben met zijn we met zijn 3en een grove planning op gaan zetten om het gehele team in te lichten met de verwachtingen/deadlines en verdere teamafspraken.