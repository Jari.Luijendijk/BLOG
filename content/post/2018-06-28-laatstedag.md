---
title: Final day of Summerschool
date: 2018-06-28
---

Vandaag ben ik vroeg mijn bed uitgegaan om alle schermen van mijn High-fid prototype uit te werken. Daarna ben ik bezig geweest om de schermen zoveel mogelijk clickable te maken.
Dit heb ik gedaan om het de gebruiker tijdens de test het zo realistisch mogelijk te maken.

Toen ik rond 13:00 op school kwam, kon ik vrijwel direct beginnen met het tentoonstellen van mijn prototype, ik gebruikte hiervoor mijn laptop met daarop de clickable interface, ook gebruikte in een korte uitleg van mijn proces en een scenario schets waarin de gebruiker zich op dit moment zou bevinden.
Dit scenario heb ik bedacht en opgeschreven, daarna heb ik dit gepresenteerd tijdens mijn High-fid prototype tests. Gelukkig was iedereen te spreken over mijn prototype wat mij zeer trots maakt.

Nadat ik 4 personen heb laten testen kwamen hier veel resultaten uit, meer dan verwacht. Ik heb thuis op mijn gemak alle resultaten geanalyseerd en er een testrapportage over geschreven met voornamelijk positieve inhoud.