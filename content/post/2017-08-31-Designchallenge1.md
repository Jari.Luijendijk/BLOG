---
title: Design Challenge 1
date: 2017-08-31
---

Op 31 augustus werden we bekend met onze eerste design challenge, en werd ons de opdracht waaraan we komende weken hard moeten gaan werken uitgelegd. Er werd verteld dat het van belang is dat er veel onderzoek plaats vindt. Daarna werden we gebriefd met meer informatie. 

- Veel onderzoek
- Speelbaar zowel online als offline
- Prototype (papertoy)
- Doelgroep
- Thema

Al snel had ik een eerste idee voor het spel en begon ik een aantal punten te noteren in mijn dummy, waar ik zowel rekening mee moest houden als dat leuk is om uit te voeren. Dit schreef ik op als vragen, omdat ik daar later onderzoek naar moet gaan doen. Mijn eerste idee was om een soort Simcity na te maken, maar dan van de stad Rotterdam. Simcity omdat dat een veel gespeeld spel is door jong en oud, en je eigenlijk oneindig door kan blijven spelen. Ik dacht meteen aan functies om waardebonnen of kortingsbonnen uit te delen door het behalen van een hoogste level.

Vragen die ik mijzelf afvroeg:

- Wie is de doelgroep?
- Wat vind de doelgroep leuk?
- Hoe leren mensen Rotterdam kennen op leuke manier?
- Hoe maak ik een motiverend spel?
- Wat zijn de offline en online functies? (wat is er mogelijk)
- Met welke thema’s binnen Rotterdam kan ik rekening houden?
- Zijn er vergelijkbare spellen waarmee ik misschien een nieuw spel van kan creëren?
    (Een aantal spellen en functies genoteerd in mijn dummy)
