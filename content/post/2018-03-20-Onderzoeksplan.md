---
title: Onderzoeksplan
date: 2018-03-20
---

Mijn taak was om in het weekend een onderzoeksplan op te stellen voor het onderwerp stress/ontspanning waar wij als groep ons op gingen focussen, aan Elliott was de taak om een onderzoeksplan op te stellen voor het onderwerp voeding.

Na het door mij gemaakte onderzoeksplan in de ochtend besproken te hebben, was het van toepassing om een aantal interview vragen op te stellen. In mijn onderzoeksplan had ik namelijk vermeld dat we het onderzoek d.m.v. een interview zouden doen,
omdat dat dichter bij de persoon is en je ook kunt doorvragen en interpreteren op de gegeven antwoorden.
Samen met Thomas hebben wij een aantal interviews gehouden op de IT-afdeling. Contact leggen ging niet altijd even gemakkelijk met de IT-studenten maar we hebben toch 4 volledige interviews kunnen afnemen.
We hebben dit opgenomen met de microfoon op de smartphone, dus konden we het later nog beluisteren om de uitkomsten te vergelijken en vast te leggen.