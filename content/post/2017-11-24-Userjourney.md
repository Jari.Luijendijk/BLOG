---
title: User-Jourey ontwikkelen
date: 2017-11-24
---

Vandaag gingen we onze User-journey ontwikkelen. Dit deden we met post-it's om situaties op te schrijven en emoties die daar bij aansluiten.
Omdat wij zelf binnen de doelgroep 16-22 vallen was het ideaal om onze eigen ervaringen erin mee te nemen.

![alt tekst](../img/uj1.jpeg)

![alt tekst](../img/uj2.jpeg)

![alt tekst](../img/uj3.jpeg)