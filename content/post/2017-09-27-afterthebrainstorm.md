---
title: After the brainstorm
date: 2017-09-27
---

Vandaag de dag dat we verder gingen na onze brainstormsessie en dat we elkaar gingen aanvullen en onze meningen gingen onderbouwen met argumenten. Ook realiseren van sommige ideeën was erg van belang, omdat alles wel haalbaar moet zijn. Zo bedachten we al mogelijkheden voor zowel offline als online functies, idividu als teamverband en bekend worden met elkaar evenals bekend worden als met Rotterdam.

![alt tekst](../img/2709.jpeg)