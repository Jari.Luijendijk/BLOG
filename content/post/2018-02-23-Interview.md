---
title: Workshop Interview voorbereiden
date: 2018-02-23
---

Ik had mij eerder deze week ingeschreven voor mijn eerste workshop in op3. Dit is namelijk "Interview voorbereiden". Tijdens de workshop kregen we veel theorie aangereken. Het was daarom aan het begin lastig om mijn aandacht erbij te houden.
Het leek me geen optie om weg te dromen dus herpakte ik mijzelf sterk door een andere (actieve) houding aan te nemen. Al snel merkte ik dat ik wat van de workshop op stak en bleek het eigenlijk heel interessant te vinden.
Na een korte tijd moesten we opdrachten gaan toepassen in praktijk. We moesten in teamverband een aantal interview criterium verzinnen om goede vragen te kunnen stellen. Met als doel om na afloop een oplossing met de gegeven antwoorden te bedenken.

Aan het einde van de workshop kregen we nog een boekwerk van A4tjes mee met tips and tricks over het voorbereiden en houden van interviews.