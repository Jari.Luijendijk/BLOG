---
title: Spelanalyse
date: 2017-09-22
---


Vandaag was het de dag om spelletjes te spelen en te analyseren, 
zo hebben we met ons team 5 verschillende spellen gespeelt waar 1 van ons een notulist was. 
We hebben ieder een interview gehouden met alle spelers die het spel hebben gespeeld en daaruit een spelanalyse gemaakt. 
Zo kwamen we er achter waar de min en plus punten van het spel zich bevinden. 
Zelf heb ik een spelanalyse gehouden over tafelvoetbal en ik moet eerlijk zeggen dat het er hard aan toe ging.

![alt tekst](../img/spelana.jpeg)

![alt tekst](../img/spelanal.jpeg)