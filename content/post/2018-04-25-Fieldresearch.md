---
title: Fieldresearch
date: 2018-04-24
---

Vandaag ben ik samen met Sem de Peinder voor de tweede keer de weg op gegaan om ons Fieldresearch beter uit te voeren. We namen een compleet andere route dan de eerste keer en kwamen zo ook op gehele andere observatiepunten uit.
We gebruikte een techniek wat gelijk is aan "kiekjes" nemen. Omdat Sem dit keer een fotocamera bij zich had gaf ik hem wel een de opdracht om ergens een foto van te maken. Zo waren we als duo-team op stap om de straten van Afrikaanderwijk nog beter te observeren, analyseren en zelfs vast te leggen.