---
title: Kwartaal terugblik
date: 2017-10-27
---

Als ik terug kijk naar dit eerste kwartaal, kijk ik terug naar vele vraagtekens.
Het was niet alleen voor mij een periode met veel onduidelijkheid, dit geldde voor meerdere mensen.
Wel kijk ik terug naar een leerzaam kwartaal, waarin ik heb geleerd om beter voor mijzelf op te komen en mijzelf meer te durven laten zien.
Aan het begin trad ik op ik de schaduw, dat hebben ik en mijn team allemaal ondervonden. Ik zou mijzelf gaan verbeteren door aanwezig te zijn binnen het team. Dat heb ik uiteindelijk ook gedaan.
Mijn team vond het een stuk prettiger dat ik zo aanwezig was en alles deelde met ze, zo was iedereen van alles op de hoogte en dat werkt een stuk aangenamer.

Voor de toekomst wil ik vanaf het begin aanwezig zijn. Me nog meer mengen binnen een overleg of discussie en alles direct op tafel gooien en met mijn teamgenoten delen.