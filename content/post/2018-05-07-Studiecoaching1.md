---
title: Hulp Competenties
date: 2018-05-07
---

Na het hoorcollege in de ochtend, ging ik naar het studiecoaching uur van Mieke. Ik was als enige aanwezig en zo kon ik fijn met Mieke in gesprek over mijn competenties.
Ik had gevraagd of ze naar mijn eerder geschreven competenties wilde kijken en daar feedback op wilde geven. En dat deed ze gelukkig terwijl ik er bij zat. Zo kon ik gemakkelijk de feedback noteren in mijn aantekeningenschrift.