---
title: Werkcollege
date: 2017-09-20
---

Vandaag kregen we een opdracht in groepsverband om 1e jaars studenten te verbinden met een deur. Hiervoor moesten we met zijn allen gaan brainstormen. Het is handig om te brainstormen over wat wel van toepassing en realiseerbaar is als wat niet van toepassing en al helemaal niet realiseerbaar is. Je komt dan op normale ideeën maar ook op hele bizarre ideeën die op het eerte gezicht nooit zouden kunnen gaan werken, maar als je daar verder op door brainstormt dat er dan uiteindelijk wel een mooi origineel idee uit kan komen rollen.

![alt tekst](../img/idea.jpeg)

![alt tekst](../img/mindmap.jpeg)

![alt tekst](../img/prototype.jpeg)

![alt tekst](../img/apparaat.jpeg)

![alt tekst](../img/deur.jpeg)