---
title: Rond om de tafel
date: 2017-09-2017
---

Vandaag gingen we een nieuw spel bedenken. Dit deden we door de creatieve techniek “brainstormen” toe te passen die ik de week ervoor had geleerd tijdens een workshop. Dit leverde deze dag veel verschillende ideeën op. We gingen erg de breedte in met ideeën en lieten iedereen zijn gang gaan om zijn idee op tafel te gooien. Er was 1 notulist die alle ideeën noteerde en 1 voorzitter die de orde bewaakte binnen de brainstormsessie. Na de breedte in te zijn geweest met de verschillende ideeën zijn we daarna de diepte in gegaan en hebben we de uitstel van oordeel toegepast met het onderbouwen van de ideeën en toevoegingen erbij plaatsen. Na heel wat divigeren waren tot een kleinere selectie gekomen van ideeën. En gingen we bij veel probleem ideeën de oplossing bedenken. Bij 1 idee kwamen we er niet uit en maakten we er een propje van, die werd weggegooit zodat we er niet meer naar omkeken

Ik had een aantal verschillende ideeën en de meeste van die waren nog overgebleven. Ook het weggegooide idee was van mij en die heb ik uiteindelijk teruggehaald en weer open gevouwen. Ik kwam met het idee om met een aantal overgebleven ideeën, 1 spel te gaan creëren. Na dat te hebben overgedragen aan de rest van mijn team waren ze het er voor meerendeels mee eens en kwamen ze al snel met wat meerdere toevoegingen en of aanpassingen.

Eerst was het lokaal te klein met onze grote meningsverschillen maar uiteindelijk waren we het allemaal met elkaar eens geworden dat we mijn ideeën moesten combineren en van een bordspel een levend spel variant moesten maken.